
<!-- Begin receipt upload -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:choose>
<c:when test="${param.hasBlob}">
  <form action="viewReceipt.do" method ="post">
    <input type="hidden" name="id" value="${param.id}">
	<input class="btn btn-success" value="View Receipt" type="submit" />
  </form>
  </c:when>
<c:otherwise>
<c:if test="${param.isPast}">
(None)
</c:if>
<c:if test="${!param.isPast}">
  <button id="addReceiptButton" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#addReceiptForm">Add Receipt</button>
  <div id="addReceiptForm" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
      	  <h4 class="modal-title">Add New Receipt</h4>
        </div>
        <form method="post" action="addReceipt.do" enctype="multipart/form-data">
          <div class="modal-body">
          	<h5>Submissions over 3500 bytes may be truncated!</p>
            <div id="receiptReimbursementId" class="form-group">
              <input type="hidden" name="id" value="${param.id}">
			</div>
			<div id="receiptFileUpload" class="form-group">
  			  <input type="file" name="dataFile" id="fileChooser" />
  			  <input type="submit" name="upload" value="Upload" />
			</div>
          </div>
          <div class="modal-footer">
          	<div class="form-group">
          	  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
          </div>
        </form>
      </div>
    </div>
  </div>
</c:if>
</c:otherwise>
</c:choose>
<!-- End receipt upload -->
