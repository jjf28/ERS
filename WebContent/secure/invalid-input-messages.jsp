

<c:if test="${sessionScope.showInvalidDollarAmount}">
  <script>
	alert("An invalid dollar amount was entered!");
  </script>
  <c:set var="showInvalidDollarAmount" scope="session" value="${false}"></c:set>
</c:if>

<c:if test="${sessionScope.showInvalidEmail}">
  <script>
	alert("An invalid email was entered!");
  </script>
  <c:set var="showInvalidEmail" scope="session" value="${false}"></c:set>
</c:if>

<c:if test="${sessionScope.showInvalidFile}">
  <script>
	alert("No valid file was given!");
  </script>
  <c:set var="showInvalidFile" scope="session" value="${false}"></c:set>
</c:if>
<c:if test="${sessionScope.showReceiptAdded}">
  <script>
	alert("Receipt added successfully!");
  </script>
  <c:set var="showReceiptAdded" scope="session" value="${false}"></c:set>
</c:if>


