

<!-- ----- new request ----- -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
  <div id="newRequestForm" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
      	  <h4 class="modal-title">New Reimbursement Request</h4>
        </div>
        <form action="newRequest.do" method="post" class="form">
          <div class="modal-body">
		    <div id="newReimbTypeDiv" class="form-group">
		      <label for="reimbtype">Reimbursement Type: </label>
			  <select class="form-control" name="reimbtype">
			    <c:forEach var="t" items="${sessionScope.reimbursementTypes}">
			      <option value="${t.type}"><c:out value="${t.type}"></c:out></option> 
	      	    </c:forEach>
			  </select>
			</div>
			<div id="newReimbAmountDiv" class="form-group">
              <label for="reimbAmount">Amount: </label>
		      <input class="form-control" name="reimbamount" type="text"></input>
			</div>
			<div id="newReimbDescDiv" class="form-group">
			  <label for="reimbDesc">Description: </label>
		      <textarea class="form-control" name="reimbdesc"></textarea>
			</div>
          </div>
          <div class="modal-footer">
          	<div class="form-group">
          	  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		      <button class="btn btn-success">Submit</button>
			</div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <button id="newRequestButton" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#newRequestForm">New Reimbursement Request</button>
<!-- ----- end new request ----- -->

