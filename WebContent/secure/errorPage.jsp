<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Error!</title>
</head>
<body>
<c:choose>
    <c:when test="${empty errorMessage}">
        There should really be an error message here!
    </c:when>
    <c:otherwise>
        Error: <b><c:out value="${errorMessage}" /></b>
    </c:otherwise>
</c:choose>
</body>
</html>