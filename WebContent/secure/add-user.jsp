

<!-- ----- new request ----- -->
  <button id="addUserButton" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#addUserForm">Add User</button>
  <div id="addUserForm" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
      	  <h4 class="modal-title">Add New User</h4>
        </div>
        <form action="addUser.do" method="post" class="form">
          <div class="modal-body">
		    <div id="newUserTypeDiv" class="form-group">
		      <label for="userType">User Type: </label>
			  <select class="form-control" name="usertype">
			    <c:forEach var="t" items="${sessionScope.userRoles}">
			      <option value="${t.role}">
			        <c:out value="${t.role}"></c:out>
			      </option> 
	      	    </c:forEach>
			  </select>
			</div>
			<div id="newFirstNameDiv" class="form-group">
              <label for="newFirstName">First Name: </label>
		      <input class="form-control" type="text" name="fname"></input>
			</div>
			<div id="newLastNameDiv" class="form-group">
              <label for="newLastName">Last Name: </label>
		      <input class="form-control" type="text" name="lname"></input>
			</div>
			<div id="newEmailDiv" class="form-group">
              <label for="newEmail">Email: </label>
		      <input class="form-control" type="text" name="email"></input>
			</div>
			<div id="newUserNameDiv" class="form-group">
              <label for="newUserName">Username: </label>
		      <input class="form-control" type="text" name="username"></input>
			</div>
			<div id="newUserPassDiv" class="form-group">
			  <label for="newUserPass">Password: </label>
			  <input class="form-control" type="password" name="password"></input>
			</div>
          </div>
          <div class="modal-footer">
          	<div class="form-group">
          	  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		      <button class="btn btn-success">Submit</button>
			</div>
          </div>
        </form>
      </div>
    </div>
  </div>
<!-- ----- end new request ----- -->

