<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Employee Reimbursements</title>
    <!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  
	<link rel="stylesheet" href="/ERS/styles.css" />
  </head>

  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top" id="topbar">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <form class="navbar-form navbar-right">
          	<a href="/ERS/index.jsp" id="logbutton" class="btn btn-success">Sign out</a>
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
      
        <div>
          <%@ include file="new-request.jsp" %>
        </div>
        
      	<div id="pending">
	      <h2>Pending Reimbursements</h2>
	      <table class="table">
	      	<tr><th>ID</th><th>Type</th><th>Description</th><th>Amount</th>
	      		<th>Submitted</th><th>Status</th></tr>
	      	<c:forEach var="t" items="${sessionScope.userPendingReimbursements}">
	      	  <tr>
	      	  	<td><c:out value="${t.id}"></c:out></td>
	      		<td><c:out value="${t.type.type}"></c:out></td>
	      		<td><c:out value="${t.description}"></c:out></td>
	      		<td><fmt:setLocale value="en_US"/>
					<fmt:formatNumber value="${t.amount}" type="currency"/></td>
				<td><fmt:formatDate type="both" value="${t.dateSubmitted}" /></td>
	      		<td><c:out value="${t.status.status}"></c:out></td>
	      		<td>
	      		  <jsp:include page="attach-receipt.jsp">
	      		    <jsp:param name="id" value="${t.id}" />
	      		    <jsp:param name="isPast" value="${false}" />
	      		    <jsp:param name="hasBlob" value="${t.receipt != null}" />
	      		  </jsp:include>
	      		</td>
	      	  </tr>
	      	</c:forEach>
	      </table>
      	</div>
      	<br />
      	<div id="finished">
      	  <h2>Past Reimbursements</h2>
      	  <table class="table">
	      	<tr><th>ID</th><th>Type</th><th>Description</th><th>Amount</th><th>Author</th>
	      	    <th>Resolver</th><th>Submitted</th><th>Resolved</th><th>Status</th></tr>
	      	<c:forEach var="t" items="${sessionScope.userPastReimbursements}">
	      	  <tr>
	      	  	<td><c:out value="${t.id}"></c:out></td>
	      		<td><c:out value="${t.type.type}"></c:out></td>
	      		<td><c:out value="${t.description}"></c:out></td>
	      		<td><fmt:setLocale value="en_US"/>
					<fmt:formatNumber value="${t.amount}" type="currency"/></td>
				<td><c:out value="${t.author.firstName} ${t.author.lastName}"></c:out></td>
		        <td><c:out value="${t.resolver.firstName} ${t.resolver.lastName}"></c:out></td>
		        <td><fmt:formatDate type="both" value="${t.dateSubmitted}" /></td>
		        <td><fmt:formatDate type="both" value="${t.dateResolved}" /></td>
	      		<td><c:out value="${t.status.status}"></c:out></td>
	      		<td><jsp:include page="attach-receipt.jsp">
	      		    <jsp:param name="id" value="${t.id}" />
	      		    <jsp:param name="isPast" value="${true}" />
	      		    <jsp:param name="hasBlob" value="${t.receipt != null}" />
	      		</jsp:include></td>
	      	  </tr>
	      	</c:forEach>
	      </table>
      	</div>
      </div>
    </div>

      <hr>
	<div>
      <footer>
        <p>&copy; 2015 ExampleCompany, Inc.</p>
      </footer>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
    
    <%@ include file="invalid-input-messages.jsp" %>
    
  </body>
</html>
