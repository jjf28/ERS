<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Expense Reimbursement - Login</title>

    <!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
	
	<!-- Latest compiled and minified JavaScript -->
	
	<link rel="stylesheet" href="styles.css" />
	
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top" id="topbar">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div id="navbar"><!--class="navbar-collapse collapse">-->
          <form action="login.do" method="post" class="navbar-form navbar-right">
            <div class="form-group">
              <input type="text" name="username" placeholder="Username" class="form-control">
            </div>
            <div class="form-group">
              <input type="password" name="password" placeholder="Password" class="form-control">
            </div>
            
            <div class="form-group">
              <input value="Sign In" type="submit" id="logbutton" class="btn btn-success" />
            </div>
            <!-- <form action="login.do" method="post">
            <a href="/ERS/secure/employee-reimbursements.jsp" id="logbutton" class="btn btn-success">Sign in</a>
			<a href="/ERS/secure/manage-reimbursements.jsp" id="logbutton" class="btn btn-success">(Temp)<!-- Replace me with
				with an appropriate redirect for employees and finance mangers after login validation -->
			</form>
			</a>
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron" id="uncolor">
      <div class="container">
        <h1>Company Expense Reimbursement</h1>
        <p>Please login to create view existing reimbursement requests.</p>
        </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
      <div class="col-md-4">
          <h2>Request an account</h2>
          <p>If you are a current employee and do not have an expense
          	reimbursement account please contact HR at
          	<a href="mailto:HR@ExampleCompanyDomain.com?Subject=New ERS Account" target="_top">HR@ExampleCompanyDomain.com</a></p>
        </div>
        <div class="col-md-4">
<!--         <form action="example.do" method ="post"> -->
<!-- 			<input type="text" name="exampleInParam" /> <br /> -->
<!-- 			<input type="submit" /> -->
<!-- 		</form> -->
        </div>
      	<div class="col-md-4">
      		<h2>What we cover</h2>
      		<p>We offer reimbursement for a wide range of of materials
      		and activities undertaken for the company. May commonly
      		reimbursed items can be found in the official <a href="faq.html">ERS FAQ</a>,
      		if your request is not listed here you may still submit
      		a request or ask your immediate supervisor for guidance.</p>
      	</div>
      </div>

      <hr>

      <footer>
        <p>&copy; 2015 ExampleCompany, Inc.</p>
      </footer>
    </div> <!-- /container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
    
    <c:if test="${sessionScope.showInvalidLogin}">
	  <script>
	    alert("Invalid username or password!");
	  </script>
 	  <c:set var="showInvalidLogin" scope="session" value="${false}"></c:set>
	</c:if>
	
  </body>
</html>
