package com.revature.daos;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public abstract class ErsDao {
	
	/**
	 * Concrete ErsDaos should return the name of the properties file containing
	 * alias to full-column-name mappings in this method
	 * @return the name of the properties with alias to full-column-name mappings
	 */
	protected abstract String getPropertyFileName();
	
	/**
	 * Concrete ErsDaos should clean up any connections they have in this method
	 */
	protected abstract void cleanup();
	
	/**
	 * Performs a null check then calls the specific ersDao's cleanup method
	 * which in turn cleans up any connections that dao may have made
	 */
	public static void cleanup(ErsDao ersDao) {
		if ( ersDao != null )
			ersDao.cleanup();
	}
	
	/** 
	 * Returns the full column name for a given column alias
	 * @param alias the alias to get the column name for
	 * @return the full column name
	 */
	protected String getFullColumnName(String alias) {
		return daoProperties.get(this.getClass()).getProperty(alias);
	}
	
	/**
	 * Constructs an ErsDao object
	 * This constructor loads the property file for the object's class (that is,
	 * the deepest sub-class of ErsDao) if it has not already been loaded
	 * @throws IOException 
	 */
	public ErsDao() throws IOException
	{
		if ( !daoProperties.containsKey(this.getClass()) )
			loadProperties(this.getClass(), getPropertyFileName());
	}
	
	public void tryCloseConnection(Connection connection) {
		if ( connection != null ) {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Gets a property object containing the column names for this table
	 * @param propertyFileName name of the file containing the column names,
	 * 		  this can include or exclude the .properties file extension
	 * @return the properties object loaded with column names if successful,
	 * 		   the properties object with no column names otherwise.
	 * @throws IOException 
	 */
	private static void loadProperties(Class<?> objectClass, String propertyFileName) throws IOException
	{	
		Properties properties = new Properties();
		if ( !propertyFileName.endsWith(propertyFileExtension) )
			propertyFileName += propertyFileExtension;
		
		String propertyFilePath = propertyFileDirectory + propertyFileName;
		properties.load(ErsDao.class.getClassLoader().getResourceAsStream(propertyFilePath));
		daoProperties.put(objectClass, properties);
	}
	
	private static String propertyFileDirectory = "properties/";
	private static String propertyFileExtension = ".properties";
	private static Map<Class<?>, Properties> daoProperties = new HashMap<Class<?>, Properties>();
	
}
