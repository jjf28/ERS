package com.revature.daos;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.NamingException;

import com.revature.beans.User;
import com.revature.beans.UserRole;
import com.revature.service.ServiceLocator;

public class UserDao extends ErsDao {
	
	private Connection connection;
	
	@Override
	protected String getPropertyFileName()
	{
		return "user";
	}

	protected void cleanup()
	{
		tryCloseConnection(connection);
		connection = null;
	}
	
	@Override
	protected void finalize()
	{
		cleanup();
	}
	
	public UserDao()
		throws SQLException, IOException, NamingException
	{
		connection = ServiceLocator.getErsConnection();
	}
	
	
	
	public User getUser(String username) {
		
		if ( username == null || username.length() <= 0 )
			return null;
		
		try {
			String sqlQuery =
				"SELECT " +
					"ERS_USERS.ERS_USERS_ID, ERS_USERS.ERS_USERNAME, ERS_USERS.ERS_PASSWORD," +
					"ERS_USERS.USER_FIRST_NAME, ERS_USERS.USER_LAST_NAME, ERS_USERS.USER_EMAIL," +
					"ERS_USER_ROLES.ERS_USER_ROLE_ID, ERS_USER_ROLES.USER_ROLE " +
				"FROM ERS_USERS INNER JOIN ERS_USER_ROLES " +
				"ON ERS_USERS.USER_ROLE_ID = ERS_USER_ROLES.ERS_USER_ROLE_ID " +
				"WHERE UPPER(ERS_USERNAME)=UPPER(?)";
		
			PreparedStatement statement = connection.prepareStatement(sqlQuery);
			statement.setString(1, username);
			ResultSet results = statement.executeQuery();
		
			User fetchedUser = null;
			if ( results.next() ) {
				fetchedUser = new User(results.getInt(1),
					results.getString(2), results.getString(3), results.getString(4),
					results.getString(5), results.getString(6),
					new UserRole(results.getInt(7), results.getString(8)));
			}
			
			return fetchedUser;
			
		} catch ( Throwable e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean createUser(User user) {
		
		try {
			String sql =
				"INSERT INTO ERS_USERS (ERS_USERS_ID,ERS_USERNAME,ERS_PASSWORD,USER_FIRST_NAME," +
					"USER_LAST_NAME,USER_EMAIL,USER_ROLE_ID) VALUES" +
					"(?, ?, ?, ?, ?, ?, ?)";
			
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, user.getId());
			statement.setString(2, user.getUsername());
			statement.setString(3, user.getPassword());
			statement.setString(4, user.getFirstName());
			statement.setString(5, user.getLastName());
			statement.setString(6, user.getEmail());
			statement.setInt(7, user.getRole().getId());
			statement.executeUpdate();
			connection.commit();
			return true;
		
		} catch ( Throwable e) {
			e.printStackTrace();
			try {
				connection.rollback();
			} catch (SQLException e1) {} // Boilerplate code
		}
		return false;
	}
	
}
