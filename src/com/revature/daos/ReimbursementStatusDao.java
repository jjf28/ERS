package com.revature.daos;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import com.revature.beans.Status;
import com.revature.service.ServiceLocator;

public class ReimbursementStatusDao extends ErsDao {
	
	private Connection connection;
	
	@Override
	protected String getPropertyFileName()
	{
		return "reimbursementStatus";
	}

	protected void cleanup()
	{
		tryCloseConnection(connection);
		connection = null;
	}
	
	@Override
	protected void finalize()
	{
		cleanup();
	}
	
	public ReimbursementStatusDao()
		throws SQLException, IOException, NamingException
	{
		connection = ServiceLocator.getErsConnection();
	}
	
	
	
	public List<Status> getReimbursementStatuses() {
		List<Status> statuses = new ArrayList<Status>();
		try {
			String sqlQuery = "SELECT REIMB_STATUS_ID, REIMB_STATUS FROM ERS_REIMBURSEMENT_STATUS";
			// Could use a regular Statement for trivially better performance
			PreparedStatement statement = connection.prepareStatement(sqlQuery);
			ResultSet results = statement.executeQuery();
			
			while ( results.next() ) {
				statuses.add(new Status(results.getInt(1), results.getString(2)));
			}
			
		} catch ( Throwable e) {
			e.printStackTrace();
		}
		return statuses;
	}

}
