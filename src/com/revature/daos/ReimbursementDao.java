package com.revature.daos;

import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import org.apache.commons.fileupload.FileItem;

import com.revature.beans.Reimbursement;
import com.revature.beans.ReimbursementType;
import com.revature.beans.Status;
import com.revature.beans.User;
import com.revature.beans.UserRole;
import com.revature.service.ServiceLocator;

public class ReimbursementDao extends ErsDao {
	
	private Connection connection;
	
	@Override
	protected String getPropertyFileName()
	{
		return "reimbursement";
	}

	protected void cleanup()
	{
		tryCloseConnection(connection);
		connection = null;
	}
	
	@Override
	protected void finalize()
	{
		cleanup();
	}
	
	public ReimbursementDao()
		throws SQLException, IOException, NamingException
	{
		connection = ServiceLocator.getErsConnection();
	}
	
	
	
	public List<Reimbursement> getReimbursements()
			throws SQLException
	{
		List<Reimbursement> reimbursements = new ArrayList<Reimbursement>();
		String sqlQuery =
			"SELECT " +
			  "REIMB_ID, REIMB_AMOUNT, REIMB_SUBMITTED, REIMB_RESOLVED, REIMB_DESCRIPTION, " +
			  "REIMB_RECEIPT, " +
    
			  "AUTH_ERS_USERS_ID, AUTH_ERS_USERNAME, AUTH_ERS_PASSWORD, AUTH_USER_FIRST_NAME, " +
			  "AUTH_USER_LAST_NAME, AUTH_USER_EMAIL, AUTH_ERS_USER_ROLE_ID, AUTH_USER_ROLE, " +

			  "RSLV_ERS_USERS_ID, RSLV_ERS_USERNAME, RSLV_ERS_PASSWORD, RSLV_USER_FIRST_NAME, " +
			  "RSLV_USER_LAST_NAME, RSLV_USER_EMAIL, RSLV_ERS_USER_ROLE_ID, RSLV_USER_ROLE, " +
    
			  "reimbStatus.REIMB_STATUS_ID, reimbStatus.REIMB_STATUS, " +
			  "reimbType.REIMB_TYPE_ID, reimbType.REIMB_TYPE " +
			"FROM " +
		      "(SELECT " +
		        "rAu.REIMB_ID REIMB_ID, rAu.REIMB_AMOUNT REIMB_AMOUNT, " +
			    "rAu.REIMB_SUBMITTED REIMB_SUBMITTED, rAu.REIMB_RESOLVED REIMB_RESOLVED, " +
			    "rAu.REIMB_DESCRIPTION REIMB_DESCRIPTION, rAu.REIMB_RECEIPT REIMB_RECEIPT, " +
			    "rAu.REIMB_STATUS_ID REIMB_STATUS_ID, rAu.REIMB_TYPE_ID REIMB_TYPE_ID, " +
    
			    "au.ERS_USERS_ID AUTH_ERS_USERS_ID, au.ERS_USERNAME AUTH_ERS_USERNAME, " +
			    "au.ERS_PASSWORD AUTH_ERS_PASSWORD, au.USER_FIRST_NAME AUTH_USER_FIRST_NAME, " +
			    "au.USER_LAST_NAME AUTH_USER_LAST_NAME, au.USER_EMAIL AUTH_USER_EMAIL, " +
    
		        "auR.ERS_USER_ROLE_ID AUTH_ERS_USER_ROLE_ID, auR.USER_ROLE AUTH_USER_ROLE " +
		      "FROM ERS_REIMBURSEMENT rAu  " +
		      "INNER JOIN ERS_USERS au " +
		        "ON au.ERS_USERS_ID = rAu.REIMB_AUTHOR " +
		      "INNER JOIN ERS_USER_ROLES auR " +
		        "ON au.USER_ROLE_ID = auR.ERS_USER_ROLE_ID) rAuR " +
		    "LEFT JOIN " +
		      "(SELECT " +
		        "re.ERS_USERS_ID RSLV_ERS_USERS_ID, re.ERS_USERNAME RSLV_ERS_USERNAME, " +
		        "re.ERS_PASSWORD RSLV_ERS_PASSWORD, re.USER_FIRST_NAME RSLV_USER_FIRST_NAME, " +
			    "re.USER_LAST_NAME RSLV_USER_LAST_NAME, re.USER_EMAIL RSLV_USER_EMAIL, " +
			    "re.USER_ROLE_ID RSLV_USER_ROLE_ID, " +
    
			    "reR.ERS_USER_ROLE_ID RSLV_ERS_USER_ROLE_ID, reR.USER_ROLE RSLV_USER_ROLE, " +
    
			    "rRe.REIMB_ID RSLV_REIMB_ID " +
		      "FROM ERS_REIMBURSEMENT rRe " +
		      "INNER JOIN ERS_USERS re " +
		        "ON re.ERS_USERS_ID = rRe.REIMB_RESOLVER " +
		      "INNER JOIN ERS_USER_ROLES reR " +
		        "ON re.USER_ROLE_ID = reR.ERS_USER_ROLE_ID) rReR " +
		      "ON rAuR.REIMB_ID = rReR.RSLV_REIMB_ID " +
		    "INNER JOIN ERS_REIMBURSEMENT_STATUS reimbStatus " +
		      "ON rAuR.REIMB_STATUS_ID = reimbStatus.REIMB_STATUS_ID " +
	        "INNER JOIN ERS_REIMBURSEMENT_TYPE reimbType " +
		      "ON rAuR.REIMB_TYPE_ID = reimbType.REIMB_TYPE_ID";
		Statement statement = connection.createStatement();
		ResultSet results = statement.executeQuery(sqlQuery);
		
		while ( results.next() ) {
			Reimbursement reimbursement = new Reimbursement(results.getInt(1), results.getDouble(2),
					results.getDate(3), results.getDate(4), results.getString(5), results.getBlob(6),
					new User(results.getInt(7), results.getString(8), results.getString(9),
							 results.getString(10), results.getString(11), results.getString(12),
							 new UserRole(results.getInt(13), results.getString(14))),
					(results.getString(16) == null ? null :
					new User(results.getInt(15), results.getString(16), results.getString(17),
							 results.getString(18), results.getString(19), results.getString(20),
							 new UserRole(results.getInt(21), results.getString(22)))),
					new Status(results.getInt(23), results.getString(24)),
					new ReimbursementType(results.getInt(25), results.getString(26))
			);
			reimbursements.add(reimbursement);
		}
		
		return reimbursements;
	}
	
	public List<Reimbursement> getReimbursementsForUser(User user)
			throws SQLException
	{
		List<Reimbursement> usersReimbursements = new ArrayList<Reimbursement>();
		String sqlQuery =
			"SELECT " +
			  "REIMB_ID, REIMB_AMOUNT, REIMB_SUBMITTED, REIMB_RESOLVED, REIMB_DESCRIPTION, " +
			  "REIMB_RECEIPT, " +
	    
			  "AUTH_ERS_USERS_ID, AUTH_ERS_USERNAME, AUTH_ERS_PASSWORD, AUTH_USER_FIRST_NAME, " +
			  "AUTH_USER_LAST_NAME, AUTH_USER_EMAIL, AUTH_ERS_USER_ROLE_ID, AUTH_USER_ROLE, " +

			  "RSLV_ERS_USERS_ID, RSLV_ERS_USERNAME, RSLV_ERS_PASSWORD, RSLV_USER_FIRST_NAME, " +
			  "RSLV_USER_LAST_NAME, RSLV_USER_EMAIL, RSLV_ERS_USER_ROLE_ID, RSLV_USER_ROLE, " +
	    
			  "reimbStatus.REIMB_STATUS_ID, reimbStatus.REIMB_STATUS, " +
			  "reimbType.REIMB_TYPE_ID, reimbType.REIMB_TYPE " +
			"FROM " +
		      "(SELECT " +
		        "rAu.REIMB_ID REIMB_ID, rAu.REIMB_AMOUNT REIMB_AMOUNT, " +
			    "rAu.REIMB_SUBMITTED REIMB_SUBMITTED, rAu.REIMB_RESOLVED REIMB_RESOLVED, " +
			    "rAu.REIMB_DESCRIPTION REIMB_DESCRIPTION, rAu.REIMB_RECEIPT REIMB_RECEIPT, " +
			    "rAu.REIMB_STATUS_ID REIMB_STATUS_ID, rAu.REIMB_TYPE_ID REIMB_TYPE_ID, " +
	    
			    "au.ERS_USERS_ID AUTH_ERS_USERS_ID, au.ERS_USERNAME AUTH_ERS_USERNAME, " +
			    "au.ERS_PASSWORD AUTH_ERS_PASSWORD, au.USER_FIRST_NAME AUTH_USER_FIRST_NAME, " +
			    "au.USER_LAST_NAME AUTH_USER_LAST_NAME, au.USER_EMAIL AUTH_USER_EMAIL, " +
	   
		        "auR.ERS_USER_ROLE_ID AUTH_ERS_USER_ROLE_ID, auR.USER_ROLE AUTH_USER_ROLE " +
		      "FROM ERS_REIMBURSEMENT rAu  " +
		      "INNER JOIN ERS_USERS au " +
		        "ON au.ERS_USERS_ID = rAu.REIMB_AUTHOR " +
		      "INNER JOIN ERS_USER_ROLES auR " +
		        "ON au.USER_ROLE_ID = auR.ERS_USER_ROLE_ID) rAuR " +
		    "LEFT JOIN " +
		      "(SELECT " +
		        "re.ERS_USERS_ID RSLV_ERS_USERS_ID, re.ERS_USERNAME RSLV_ERS_USERNAME, " +
		        "re.ERS_PASSWORD RSLV_ERS_PASSWORD, re.USER_FIRST_NAME RSLV_USER_FIRST_NAME, " +
			    "re.USER_LAST_NAME RSLV_USER_LAST_NAME, re.USER_EMAIL RSLV_USER_EMAIL, " +
			    "re.USER_ROLE_ID RSLV_USER_ROLE_ID, " +
	    
			    "reR.ERS_USER_ROLE_ID RSLV_ERS_USER_ROLE_ID, reR.USER_ROLE RSLV_USER_ROLE, " +
	    
			    "rRe.REIMB_ID RSLV_REIMB_ID " +
		      "FROM ERS_REIMBURSEMENT rRe " +
		      "INNER JOIN ERS_USERS re " +
		        "ON re.ERS_USERS_ID = rRe.REIMB_RESOLVER " +
		      "INNER JOIN ERS_USER_ROLES reR " +
		        "ON re.USER_ROLE_ID = reR.ERS_USER_ROLE_ID) rReR " +
		      "ON rAuR.REIMB_ID = rReR.RSLV_REIMB_ID " +
		    "INNER JOIN ERS_REIMBURSEMENT_STATUS reimbStatus " +
		      "ON rAuR.REIMB_STATUS_ID = reimbStatus.REIMB_STATUS_ID " +
	        "INNER JOIN ERS_REIMBURSEMENT_TYPE reimbType " +
		      "ON rAuR.REIMB_TYPE_ID = reimbType.REIMB_TYPE_ID";
		Statement statement = connection.createStatement();
		ResultSet results = statement.executeQuery(sqlQuery);
		
		while ( results.next() ) {
			Reimbursement reimbursement = new Reimbursement(results.getInt(1), results.getDouble(2),
					results.getDate(3), results.getDate(4), results.getString(5), results.getBlob(6),
					new User(results.getInt(7), results.getString(8), results.getString(9),
							 results.getString(10), results.getString(11), results.getString(12),
							 new UserRole(results.getInt(13), results.getString(14))),
					(results.getString(16) == null ? null :
					new User(results.getInt(15), results.getString(16), results.getString(17),
							 results.getString(18), results.getString(19), results.getString(20),
							 new UserRole(results.getInt(21), results.getString(22)))),
					new Status(results.getInt(23), results.getString(24)),
					new ReimbursementType(results.getInt(25), results.getString(26))
			);
			usersReimbursements.add(reimbursement);
		}
		return usersReimbursements;
	}
	
	public boolean addBlobToReimbursement(Reimbursement reimbursement, String fileName, FileItem file) {
		
		try {
			Blob blob = connection.createBlob();
			if ( fileName.contains("/") )
				return false;
			String headerInfo = fileName.concat("/");
			
			byte[] u8charHeader = new byte[headerInfo.length()];
			for ( int i=0; i<headerInfo.length(); i++ ) {
				u8charHeader[i] = (byte)headerInfo.charAt(i);
			}
			blob.setBytes(1L, u8charHeader);
			blob.setBytes(1L+headerInfo.length(), file.get());
			//OutputStream out = blob.(1L);
			//out.write(file.get());
			reimbursement.setReceipt(blob);
			return updateReimbursement(reimbursement);
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean updateReimbursement(Reimbursement reimbursement) {

		String sql = "UPDATE ERS_REIMBURSEMENT " +
					 "SET " +
					    "REIMB_ID = ?, " +
					    "REIMB_AMOUNT = ?, " +
					    "REIMB_SUBMITTED = ?, " +
					    "REIMB_RESOLVED = ?, " +
					    "REIMB_DESCRIPTION = ?, " +
					    "REIMB_RECEIPT = ?, " +
					    "REIMB_AUTHOR = ?, " +
					    "REIMB_RESOLVER = ?, " +
					    "REIMB_STATUS_ID = ?, " +
					    "REIMB_TYPE_ID = ? " +
					 "WHERE REIMB_ID = ?";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, reimbursement.getId());
			preparedStatement.setDouble(2, reimbursement.getAmount());
			preparedStatement.setDate(3, reimbursement.getDateSubmitted());
			preparedStatement.setDate(4, reimbursement.getDateResolved());
			preparedStatement.setString(5, reimbursement.getDescription());
			preparedStatement.setBlob(6, reimbursement.getReceipt());
			preparedStatement.setInt(7, reimbursement.getAuthor().getId());
			if ( reimbursement.getResolver() == null )
				preparedStatement.setNull(8, java.sql.Types.NUMERIC);
			else
				preparedStatement.setInt(8, reimbursement.getResolver().getId());
			preparedStatement.setInt(9, reimbursement.getStatus().getId());
			preparedStatement.setInt(10, reimbursement.getType().getId());
			preparedStatement.setInt(11, reimbursement.getId());
			preparedStatement.executeUpdate();
			connection.commit();
			return true;
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return false;
	}

	public boolean addReimbursement(Reimbursement reimbursement) {
		
		if ( reimbursement.isResolved() )
			throw new UnsupportedOperationException();
		
		String sql =
			"INSERT INTO ERS_REIMBURSEMENT (REIMB_ID,REIMB_AMOUNT,REIMB_SUBMITTED, " +
			"REIMB_RESOLVED,REIMB_DESCRIPTION,REIMB_RECEIPT,REIMB_AUTHOR,REIMB_RESOLVER, " +
			"REIMB_STATUS_ID,REIMB_TYPE_ID) VALUES " +
			"(?, ?, CURRENT_TIMESTAMP, null, ?, ?, " +
			"?, null, ?, ?)";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, reimbursement.getId());
			preparedStatement.setDouble(2, reimbursement.getAmount());
			preparedStatement.setString(3, reimbursement.getDescription());
			preparedStatement.setBlob(4, reimbursement.getReceipt());
			preparedStatement.setInt(5, reimbursement.getAuthor().getId());
			preparedStatement.setInt(6, reimbursement.getStatus().getId());
			preparedStatement.setInt(7, reimbursement.getType().getId());
			preparedStatement.executeUpdate();
			connection.commit();
			return true;
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return false;
	}
	
}
