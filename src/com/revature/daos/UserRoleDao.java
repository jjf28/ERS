package com.revature.daos;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import com.revature.beans.UserRole;
import com.revature.service.ServiceLocator;

public class UserRoleDao extends ErsDao {

	
	private Connection connection;
	
	@Override
	protected String getPropertyFileName()
	{
		return "userRoles";
	}

	protected void cleanup()
	{
		tryCloseConnection(connection);
		connection = null;
	}
	
	@Override
	protected void finalize()
	{
		cleanup();
	}
	
	public UserRoleDao()
		throws SQLException, IOException, NamingException
	{
		connection = ServiceLocator.getErsConnection();
	}
	
	
	
	public List<UserRole> getUserRoles() {
		
		List<UserRole> roles = new ArrayList<UserRole>();
		try {
			String sqlQuery = "SELECT ERS_USER_ROLE_ID, USER_ROLE FROM ERS_USER_ROLES";
			// Could use a regular Statement for trivially better performance
			PreparedStatement statement = connection.prepareStatement(sqlQuery);
			ResultSet results = statement.executeQuery();
			
			while ( results.next() ) {
				roles.add(new UserRole(results.getInt(1), results.getString(2)));
			}
			
		} catch ( Throwable e) {
			e.printStackTrace();
		}
		return roles;
	}
	

}
