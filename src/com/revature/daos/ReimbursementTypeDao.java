package com.revature.daos;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import com.revature.beans.ReimbursementType;
import com.revature.service.ServiceLocator;

public class ReimbursementTypeDao extends ErsDao {
	
	private Connection connection;
	
	@Override
	protected String getPropertyFileName()
	{
		return "reimbursementType";
	}

	protected void cleanup()
	{
		tryCloseConnection(connection);
		connection = null;
	}
	
	@Override
	protected void finalize()
	{
		cleanup();
	}
	
	public ReimbursementTypeDao()
		throws SQLException, IOException, NamingException
	{
		connection = ServiceLocator.getErsConnection();
	}
	
	
	
	public List<ReimbursementType> getReimbursementTypes() {
		
		List<ReimbursementType> types = new ArrayList<ReimbursementType>();
		try {
			String sqlQuery = "SELECT REIMB_TYPE_ID, REIMB_TYPE FROM ERS_REIMBURSEMENT_TYPE";
			// Could use a regular Statement for trivially better performance
			PreparedStatement statement = connection.prepareStatement(sqlQuery);
			ResultSet results = statement.executeQuery();
			
			while ( results.next() ) {
				types.add(new ReimbursementType(results.getInt(1), results.getString(2)));
			}
			
		} catch ( Throwable e) {
			e.printStackTrace();
		}
		return types;
	}

}
