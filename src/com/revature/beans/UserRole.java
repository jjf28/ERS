package com.revature.beans;

public class UserRole {
	private int id;
	private String role;
	
	@Override
	public String toString() {
		return "UserRole [id=" + id + ", role=" + role + "]";
	}
	public UserRole() {
		super();
	}
	public UserRole(int id, String role) {
		super();
		this.id = id;
		this.role = role;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public boolean isManager() {
		return this != null && role != null && role.equalsIgnoreCase("Manager");
	}
}
