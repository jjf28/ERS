package com.revature.service;

import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.sql.DataSource;

/* This is buggy and not currently in use */
public class DataService {
	
	// Dependency injection w/ JNDI
	@Resource(name="db/ers")
	private DataSource dataSource;
	//private Connection connection;
	
	public Connection getErsDatabaseConnection() {
		try {
			return dataSource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
