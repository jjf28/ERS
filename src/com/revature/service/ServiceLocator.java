package com.revature.service;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ServiceLocator {

	private static DataSource getReimbursementDatabase()
			throws IOException, NamingException
	{
		Properties enviornmentProps = new Properties();
		enviornmentProps.load(
				ServiceLocator.class.getClassLoader()
					.getResourceAsStream("properties/jndi.properties")
		);
		Context context = new InitialContext(enviornmentProps);
		return (DataSource)context.lookup("db/ers");
	}
	
	public static Connection getErsConnection()
			throws SQLException, IOException, NamingException
	{
		DataSource dataSource = getReimbursementDatabase();
		if ( dataSource != null ) {
			Connection connection = dataSource.getConnection();
			connection.setAutoCommit(false);
			return connection;
		}
		return null;
	}
}
