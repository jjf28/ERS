package com.revature.business;

import java.util.ArrayList;
import java.util.List;

import com.revature.beans.Reimbursement;
import com.revature.beans.User;

public class ReimbursementFilter {

	public static List<Reimbursement> getPendingReimbursements(List<Reimbursement> reimbursements) {
		
		List<Reimbursement> pendingReimbursements = new ArrayList<Reimbursement>();
		for ( Reimbursement r : reimbursements ) {
			if ( r.getStatus().isPending() ) {
				pendingReimbursements.add(r);
			}
		}
		return pendingReimbursements;
	}
	
	public static List<Reimbursement> getPastReimbursements(List<Reimbursement> reimbursements) {
		
		List<Reimbursement> pastReimbursements = new ArrayList<Reimbursement>();
		for ( Reimbursement r : reimbursements ) {
			if ( !r.getStatus().isPending() ) {
				pastReimbursements.add(r);
			}
		}
		return pastReimbursements;
	}
	
	public static List<Reimbursement> getUsersPendingReimbursements(
			List<Reimbursement> reimbursements, User user) {
		
		List<Reimbursement> pendingReimbursements = new ArrayList<Reimbursement>();
		for ( Reimbursement r : reimbursements ) {
			if ( r.getStatus().isPending() && r.getAuthor().getId() == user.getId() ) {
				pendingReimbursements.add(r);
			}
		}
		return pendingReimbursements;
	}
	
	public static List<Reimbursement> getUsersPastReimbursements(
			List<Reimbursement> reimbursements, User user) {
		
		List<Reimbursement> pastReimbursements = new ArrayList<Reimbursement>();
		for ( Reimbursement r : reimbursements ) {
			if ( !r.getStatus().isPending() && r.getAuthor().getId() == user.getId() ) {
				pastReimbursements.add(r);
			}
		}
		return pastReimbursements;
	}
}
