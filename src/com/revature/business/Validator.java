package com.revature.business;

public class Validator {

	public static boolean isValidEmail(String email) {
		
		return
			 email != null &&
			 email.indexOf('@') == email.lastIndexOf('@') &&
			 email.indexOf('.') == email.lastIndexOf('.') &&
			 email.indexOf('@') > 0 &&
			 email.indexOf('.') > email.indexOf('@')+1 &&
			 email.indexOf('.') < email.length()-1;
	}
	
	public static boolean isValidDollarAmount(String dollarAmount) {
		
		return
			dollarAmount != null &&
			dollarAmount.matches("^[+-]?[0-9]{1,3}(?:,?[0-9]{3})*(?:\\.[0-9]{2})?$");
	}
}
