package com.revature.business;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;

import com.revature.beans.UserRole;
import com.revature.daos.ErsDao;
import com.revature.daos.UserRoleDao;

public class UserRoleFactory {
	public static UserRole makeUserRole(String userRoleString) throws SQLException, IOException, NamingException {
		UserRoleDao userRoleDao = null;
		try {
			userRoleDao = new UserRoleDao();
			List<UserRole> userRoles = userRoleDao.getUserRoles();
			for ( UserRole t : userRoles ) {
				if ( userRoleString.equalsIgnoreCase(t.getRole()) )
					return t;
			}
			return null;
		} finally {
			ErsDao.cleanup(userRoleDao);
		}
	}
}
