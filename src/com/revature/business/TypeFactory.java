package com.revature.business;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;

import com.revature.beans.ReimbursementType;
import com.revature.daos.ErsDao;
import com.revature.daos.ReimbursementTypeDao;

public class TypeFactory {
	public static ReimbursementType makeType(String typeString) throws SQLException, IOException, NamingException {
		ReimbursementTypeDao typeDao = null;
		try {
			typeDao = new ReimbursementTypeDao();
			List<ReimbursementType> reimbursementTypes = typeDao.getReimbursementTypes();
			for ( ReimbursementType t : reimbursementTypes ) {
				if ( typeString.equalsIgnoreCase(t.getType()) )
					return t;
			}
			return null;
		} finally {
			ErsDao.cleanup(typeDao);
		}
	}
}
