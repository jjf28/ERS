package com.revature.business;

import com.revature.beans.User;
import com.revature.daos.ErsDao;
import com.revature.daos.UserDao;

public class Authenticator {

	public static boolean validateCredentials(String username, String password) {
		UserDao userDao = null;
		try {
			userDao = new UserDao();
			User dbUser = userDao.getUser(username);
			if ( dbUser == null )
				return false;
			
			String dbUsername = dbUser.getUsername();
			String dbPasswordHash = dbUser.getPassword();
			return username.equalsIgnoreCase(dbUsername) &&
				   BCrypt.checkpw(password, dbPasswordHash); 
		
		} catch (Throwable t) {
			t.printStackTrace();
		} finally {
			ErsDao.cleanup(userDao);
		}
		return false;
	}
	
	public static String getPasswordHash(String password) {
		
		return BCrypt.hashpw(password, BCrypt.gensalt());
	}
}
