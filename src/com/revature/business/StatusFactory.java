package com.revature.business;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;

import com.revature.beans.Status;
import com.revature.daos.ErsDao;
import com.revature.daos.ReimbursementStatusDao;

public class StatusFactory {

	private static String pendingName = "Pending";
	private static String approvedName = "Approved";
	private static String deniedName = "Denied";
	protected static Status makeStatus(String statusName) throws SQLException {
		
		ReimbursementStatusDao statusDao = null;
		try {
			statusDao = new ReimbursementStatusDao();
			List<Status> statuses = statusDao.getReimbursementStatuses();
			for ( Status s : statuses ) {
				if ( s.getStatus().equalsIgnoreCase(statusName) )
					return s;
			}
		} catch (IOException | NamingException e) {
			e.printStackTrace();
		}
		finally {
			ErsDao.cleanup(statusDao);
		}
		return null;
	}
	
	public static Status makePending() throws SQLException {
		return makeStatus(pendingName);
	}
	public static Status makeApproved() throws SQLException {
		return makeStatus(approvedName);
	}
	public static Status makeDenied() throws SQLException {
		return makeStatus(deniedName);
	}
}
