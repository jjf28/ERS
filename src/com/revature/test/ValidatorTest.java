package com.revature.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.revature.business.Validator;

public class ValidatorTest {

	@Test
	public void test() {
		assertTrue(true);
	}

	@Test
	public void testEmailValidation() {
		
		assertTrue(Validator.isValidEmail("blah@gmail.com"));
		assertTrue(Validator.isValidEmail("ab@qwerty.net"));
		assertTrue(Validator.isValidEmail("someemail@dom.uk"));
		
		assertFalse(Validator.isValidEmail(""));
		assertFalse(Validator.isValidEmail("@."));
		assertFalse(Validator.isValidEmail("a@b."));
		assertFalse(Validator.isValidEmail("@b.c"));
		assertFalse(Validator.isValidEmail("a@.c"));
		assertFalse(Validator.isValidEmail("blahgmail.com"));
		assertFalse(Validator.isValidEmail("ab@qwerty"));
	}
}
