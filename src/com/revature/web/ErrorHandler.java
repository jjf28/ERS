package com.revature.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// TODO: Can make better, see http://www.tutorialspoint.com/servlets/servlets-exception-handling.htm

public class ErrorHandler extends HttpServlet {
	private static final long serialVersionUID = -764164854298916400L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		resp.getWriter().println("ErrorHandler was called on a doGet() request, " +
				"something throwable must have been thrown!");
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		resp.getWriter().println("ErrorHandler was called on a doPost() request, " +
				"something throwable must have been thrown!");
	}
}
