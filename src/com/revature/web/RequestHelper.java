package com.revature.web;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.revature.beans.Reimbursement;
import com.revature.beans.ReimbursementType;
import com.revature.beans.Status;
import com.revature.beans.User;
import com.revature.beans.UserRole;
import com.revature.business.Authenticator;
import com.revature.business.ReimbursementFilter;
import com.revature.business.StatusFactory;
import com.revature.business.TypeFactory;
import com.revature.business.UserRoleFactory;
import com.revature.business.Validator;
import com.revature.daos.ErsDao;
import com.revature.daos.ReimbursementDao;
import com.revature.daos.ReimbursementTypeDao;
import com.revature.daos.UserDao;
import com.revature.daos.UserRoleDao;

public class RequestHelper {

	private HttpServletRequest request;
	private HttpServletResponse response;
	private String manageReimbursementsName = "manage-reimbursements.jsp";
	private String employeeReimbursementsName = "employee-reimbursements.jsp";
	private String managerReimbursementsPath = "/ERS/secure/" + manageReimbursementsName;
	private String employeeReimbursementsPath = "/ERS/secure/" + employeeReimbursementsName;
	
	public RequestHelper(HttpServletRequest request, HttpServletResponse response) {
		
		this.request = request;
		this.response = response;
	}
	public void forward(String target) {
		
		try {
			request.getRequestDispatcher(target).forward(request, response);
		} catch (ServletException e) {
			e.printStackTrace();
			errorPage("There was an error forwarding you to a new page, a SevletException was thrown!");
		} catch (IOException e) {
			e.printStackTrace();
			errorPage("There was an error connecting to the ERS database, an IOException was thrown!");
		}
	}
	
	public void redirect(String target) {
		
		try {
			response.sendRedirect(target);
		} catch (IOException e) {
			e.printStackTrace();
			errorPage("There was an error redirecting you to a new page, an IOException was thrown!");
		}
	}
	public void errorPage(String errorMessage) {
		
		request.setAttribute("errorMessage", errorMessage);
		forward("errorPage.jsp");
	}
	
	public String getParam(String name) {
		return request.getParameter(name);
	}
	public Object getSessionAttribute(String name) {
		return request.getSession().getAttribute(name);
	}
	public void setSessionAttribute(String name, Object value) {
		request.getSession().setAttribute(name, value);
	}

	public User getCurrUser() {
		
		Object currUserObj = getSessionAttribute("userInfo");
		if ( currUserObj instanceof User ) {
			return (User)currUserObj;
		}
		return null;
	}
	public boolean currUserIsManager() {
		return getCurrUser().getRole().isManager();
	}
	@SuppressWarnings("unchecked")
	public List<Reimbursement> getCachedPendingReimbursements() {
		Object pendingReimbursementsObj = currUserIsManager() ?
				getSessionAttribute("allPendingReimbursements") :
				getSessionAttribute("userPendingReimbursements");
			
		if ( pendingReimbursementsObj instanceof List<?> ) {
			List<?> pendingReimbursementsAnonList = (List<?>)pendingReimbursementsObj;
			if ( pendingReimbursementsAnonList.size() > 0 &&
				 pendingReimbursementsAnonList.get(0) instanceof Reimbursement ) {
				return (List<Reimbursement>)pendingReimbursementsAnonList;
			}
			
		}
		return new ArrayList<Reimbursement>();
	}
	public Reimbursement getCachedPendingReimbursement(int id) {
		List<Reimbursement> cachedPendingReimbursements = getCachedPendingReimbursements();
		for ( Reimbursement r : cachedPendingReimbursements ) {
			if ( r.getId() == id )
				return r;
		}
		return null;
	}
	@SuppressWarnings("unchecked")
	public Reimbursement getCachedReimbursement(int id) {
		List<Reimbursement> pendingReimbursements = currUserIsManager() ?
				(List<Reimbursement>)getSessionAttribute("allPendingReimbursements") :
				(List<Reimbursement>)getSessionAttribute("userPendingReimbursements");
		List<Reimbursement> pastReimbursements = currUserIsManager() ?
				(List<Reimbursement>)getSessionAttribute("allPastReimbursements") :
				(List<Reimbursement>)getSessionAttribute("userPastReimbursements");
		for ( Reimbursement r : pendingReimbursements ) {
			if ( r.getId() == id )
				return r;
		}
		for ( Reimbursement r : pastReimbursements ) { 
			if ( r.getId() == id )
				return r;
		}
		return null;
	}
	
	public void cacheReimbursements() {
		ReimbursementDao reimbursementDao = null;
		try {
			reimbursementDao = new ReimbursementDao();
			List<Reimbursement> reimbursements = currUserIsManager() ?
					reimbursementDao.getReimbursements() :
					reimbursementDao.getReimbursementsForUser(getCurrUser());
					
			if ( currUserIsManager() ) {
				setSessionAttribute("allPendingReimbursements",
					ReimbursementFilter.getPendingReimbursements(reimbursements));
				setSessionAttribute("allPastReimbursements",
					ReimbursementFilter.getPastReimbursements(reimbursements));
			}
			setSessionAttribute("userPendingReimbursements",
				ReimbursementFilter.getUsersPendingReimbursements(reimbursements, getCurrUser()));
			setSessionAttribute("userPastReimbursements",
				ReimbursementFilter.getUsersPastReimbursements(reimbursements, getCurrUser()));
		} catch (SQLException e) {
			e.printStackTrace();
			errorPage("There was an error processing a SQL request, a SQLException was thrown!");
		} catch (IOException e) {
			e.printStackTrace();
			errorPage("There was an error connecting to the ERS database, an IOException was thrown!");
		} catch (NamingException e) {
			e.printStackTrace();
			errorPage("The database name was invalid, a NamingException was thrown!");
		} finally {
			ErsDao.cleanup(reimbursementDao);
		}
	}
	public void loadReimbursementPage() {
		
		cacheReimbursements();
		if ( currUserIsManager() )
			redirect(managerReimbursementsPath);
		else
			redirect(employeeReimbursementsPath);
	}
	public void refreshReimbursementPage() {
		
		cacheReimbursements();
		if ( currUserIsManager() )
			redirect(manageReimbursementsName);
		else
			redirect(employeeReimbursementsName);
	}
	public void refreshLoginPage() {
		redirect("/ERS");
	}
	
	public void exampleRequest() {
		
		// 1. validate the input
		// 2. Beanify the input
		// 3. Call business delegate
		// 4. Persist logic (request, session, application variables)
		
		/*String testMessage = request.getParameter("exampleInParam");
		System.out.println("testMessage: " + testMessage);
		if ( testMessage != null && testMessage.length() > 0 ) {
			// TestBean testBean = new TestBean(testMessage);
			// new BusinessDelegate().blah(testBean);
			setSessionAttribute("exampleOutParam", testMessage);
			//return "example.jsp";
			errorPage(testMessage);
		}
		else
			errorPage("Invalid test message!");*/
	}
	
	public void login() {
		
		UserDao userDao = null;
		UserRoleDao userRolesDao = null;
		ReimbursementTypeDao typesDao = null;
		try {
			userDao = new UserDao();
			typesDao = new ReimbursementTypeDao();
			String username = request.getParameter("username");
			String password = request.getParameter("password");
			
			if ( Authenticator.validateCredentials(username, password) ) {
				
				setSessionAttribute("loggedIn", true);
				setSessionAttribute("userInfo", userDao.getUser(username));
				setSessionAttribute("reimbursementTypes", typesDao.getReimbursementTypes());
				if ( currUserIsManager() ) {
					userRolesDao = new UserRoleDao();
					setSessionAttribute("userRoles", userRolesDao.getUserRoles());
				}
				loadReimbursementPage();
			}
			else {
				setSessionAttribute("showInvalidLogin", true);
				refreshLoginPage();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			errorPage("SQLException!");
		} catch (Exception e) {
			e.printStackTrace();
			errorPage("Unknown login exception occured!");
		} finally {
			ErsDao.cleanup(userDao);
			ErsDao.cleanup(typesDao);
			ErsDao.cleanup(userRolesDao);
		}
	}
	public void logout() {
		
		request.getSession().invalidate(); // delete ALL session data
		redirect("/ERS");
	}
	
	public void addUser() {
		String sUserType = request.getParameter("usertype");
		String sFirstName = request.getParameter("fname");
		String sLastName = request.getParameter("lname");
		String sEmail = request.getParameter("email");
		String sUsername = request.getParameter("username");
		String sPassword = request.getParameter("password");
		
		if ( !Validator.isValidEmail(sEmail) ) {
			setSessionAttribute("showInvalidEmail", true);
			this.refreshReimbursementPage();
			return;
		}
		
		UserDao userDao = null;
		try {
			userDao = new UserDao();
			
			UserRole userRole = UserRoleFactory.makeUserRole(sUserType);
			String passwordHash = Authenticator.getPasswordHash(sPassword);
			User user = new User(-1, sUsername, passwordHash,
					sFirstName, sLastName, sEmail, userRole);
			
			if ( userDao.createUser(user) )
				refreshReimbursementPage();
			else
				errorPage("Failed to create user due to DAO issues!");
			
		} catch (SQLException e) {
			e.printStackTrace();
			errorPage("There was an error processing a SQL request, a SQLException was thrown!");
		} catch (IOException e) {
			e.printStackTrace();
			errorPage("There was an error connecting to the ERS database, an IOException was thrown!");
		} catch (NamingException e) {
			e.printStackTrace();
			errorPage("The database name was invalid, a NamingException was thrown!");
		} finally {
			ErsDao.cleanup(userDao);
		}
	}
	public void newReimbRequest() {
		
		String sReimbursementType = request.getParameter("reimbtype");
		String sReimbursementAmount = request.getParameter("reimbamount");
		String sReimbursementDescription = request.getParameter("reimbdesc");
		
		if ( !Validator.isValidDollarAmount(sReimbursementAmount) ) {
			setSessionAttribute("showInvalidDollarAmount", true);
			this.refreshReimbursementPage();
			return;
		}
		
		Object currUserObj = getSessionAttribute("userInfo");
		if ( currUserObj instanceof User ) {
			User currUser = (User)currUserObj;
			ReimbursementDao reimbursementDao = null;
			try {
				reimbursementDao = new ReimbursementDao();
				ReimbursementType reimbursementType = TypeFactory.makeType(sReimbursementType);
				Reimbursement reimbursement = new Reimbursement(-1, Double.parseDouble(sReimbursementAmount),
					null, null, sReimbursementDescription, null, currUser, null, new Status(0, "Pending"),
					reimbursementType);
				
				reimbursementDao.addReimbursement(reimbursement);
				refreshReimbursementPage();
				
			} catch (SQLException e) {
				e.printStackTrace();
				errorPage("There was an error processing a SQL request, a SQLException was thrown!");
			} catch (IOException e) {
				e.printStackTrace();
				errorPage("There was an error connecting to the ERS database, an IOException was thrown!");
			} catch (NumberFormatException e) {
				e.printStackTrace();
				errorPage("An improper number format was submitted, a NumberFormatException was thrown!");
			} catch (NamingException e) {
				e.printStackTrace();
				errorPage("The database name was invalid, a NamingException was thrown!");
			} finally {
				ErsDao.cleanup(reimbursementDao);
			}
		}
	}
	@SuppressWarnings("unchecked")
	public void addReceipt() {
		List<FileItem> fileItems;
		try {
			fileItems = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
			
			String uploadedFileName = "";
			FileItem uploadedFile = null;
			int id = -1;
			for( FileItem fileItem : fileItems ) {
				if( !fileItem.isFormField() ) {
					uploadedFile = fileItem;
					uploadedFileName = fileItem.getName();
				} else {
					if ( fileItem.getFieldName().equals("id") ) {
						try {
							id = Integer.parseInt(fileItem.getString());
						} catch ( Exception e ) {
							e.printStackTrace();
							errorPage("The id given was invalid, an exception was thrown!");
							return;
						}
					}
				}
			}
			if ( id >= 0 && uploadedFileName.length() > 0 &&
				 uploadedFile != null && uploadedFile.getSize() > 0 )
			{
				ReimbursementDao reimbursementDao = null;
				try {
					Reimbursement reimbursement = getCachedPendingReimbursement(id);
					reimbursementDao = new ReimbursementDao();
					if ( reimbursementDao.addBlobToReimbursement(reimbursement,
							uploadedFileName, uploadedFile) )
					{
						setSessionAttribute("showReceiptAdded", true);
						this.refreshReimbursementPage();
						return;
					}
				} catch (SQLException e) {
					e.printStackTrace();
					errorPage("There was an error processing a SQL request, a SQLException was thrown!");
				} catch (IOException e) {
					e.printStackTrace();
					errorPage("There was an error connecting to the ERS database, an IOException was thrown!");
				} catch (NamingException e) {
					e.printStackTrace();
					errorPage("The database name was invalid, a NamingException was thrown!");
				} finally {
					ErsDao.cleanup(reimbursementDao);
				}
			}
			else
			{
				setSessionAttribute("showInvalidFile", true);
				this.refreshReimbursementPage();
				return;
			}
			
		} catch (FileUploadException e) {
			e.printStackTrace();
		}
	}
	public void viewReceipt() {
		int id = -1;
		InputStream inputStream = null;
		OutputStream outStream = null;
		try {
			id = Integer.parseInt(getParam("id"));
			
			Reimbursement reimbursement = getCachedReimbursement(id);
			Blob blob = reimbursement.getReceipt();
			inputStream = blob.getBinaryStream();
			int fileLength = inputStream.available();
			
			byte[] headerAndFileBuffer = new byte[(int)blob.length()];
			int bytesRead = -1;
         
			int bodyStart = 0;
			int pos = 0;
			while ((bytesRead = inputStream.read(headerAndFileBuffer)) != -1) {
				if ( bodyStart == 0 ) {
					for ( int i=pos; i<bytesRead; i++ ) {
						byte character = headerAndFileBuffer[i];
						if ( ((char)character) == '/' ) {
							bodyStart = i+1;
							break;
						}
					}
					pos += bytesRead;
				}
			}
			inputStream.close();
			
			String fileName = "";
			for ( int i=0; i<(bodyStart-1); i++ ) {
				byte character = headerAndFileBuffer[i];
				fileName += (char)character;
			}

			ServletContext context = request.getServletContext();

			// sets MIME type for the file download
			String mimeType = context.getMimeType(fileName);
			if (mimeType == null) {        
				mimeType = "application/octet-stream";
			}
         
			// set content properties and header attributes for the response
			response.setContentType(mimeType);
			response.setContentLength(fileLength-bodyStart);
			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"", fileName);
			response.setHeader(headerKey, headerValue);
			
			outStream = response.getOutputStream();
			outStream.write(headerAndFileBuffer, bodyStart, fileLength-bodyStart);
			outStream.close();
			return;
        
		} catch ( Exception e ) {
			e.printStackTrace();
			errorPage("File could not be fetched!");
			return;
		}
	}
	
	public void resolve() {
		try {
			
			int id = Integer.parseInt(getParam("id"));
			boolean isApproving = getParam("approve") != null;
			boolean isDenying = getParam("deny") != null;
			
			Reimbursement reimbursement = getCachedPendingReimbursement(id);
			if ( isApproving && !isDenying )
				approveRequest(reimbursement);
			else if ( isDenying && !isApproving )
				denyRequest(reimbursement);
			
			refreshReimbursementPage();
		
		} catch ( Exception e ) {
			e.printStackTrace();
			errorPage("Failed to create new reimbursement, an Exception was thrown!");
		}
	}
	public boolean approveRequest(Reimbursement reimbursement) {
		ReimbursementDao reimbursementDao = null;
		try {
			reimbursement.setStatus(StatusFactory.makeApproved());
			reimbursement.setResolver(getCurrUser());
			reimbursement.setDateResolved(new Date(Calendar.getInstance().getTimeInMillis()));
			reimbursementDao = new ReimbursementDao();
			reimbursementDao.updateReimbursement(reimbursement);
			
		} catch (SQLException e) {
			e.printStackTrace();
			errorPage("There was an error processing a SQL request, a SQLException was thrown!");
		} catch (IOException e) {
			e.printStackTrace();
			errorPage("There was an error connecting to the ERS database, an IOException was thrown!");
		} catch (NamingException e) {
			e.printStackTrace();
			errorPage("The database name was invalid, a NamingException was thrown!");
		} finally {
			ErsDao.cleanup(reimbursementDao);
		}
		return false;
	}
	public boolean denyRequest(Reimbursement reimbursement) {
		ReimbursementDao reimbursementDao = null;
		try {
			reimbursement.setStatus(StatusFactory.makeDenied());
			reimbursement.setResolver(getCurrUser());
			reimbursement.setDateResolved(new Date(Calendar.getInstance().getTimeInMillis()));
			reimbursementDao = new ReimbursementDao();
			reimbursementDao.updateReimbursement(reimbursement);
			
		} catch (SQLException e) {
			e.printStackTrace();
			errorPage("There was an error processing a SQL request, a SQLException was thrown!");
		} catch (IOException e) {
			e.printStackTrace();
			errorPage("There was an error connecting to the ERS database, an IOException was thrown!");
		} catch (NamingException e) {
			e.printStackTrace();
			errorPage("The database name was invalid, a NamingException was thrown!");
		} finally {
			ErsDao.cleanup(reimbursementDao);
		}
		return false;
	}
	
	public void updateShown() {
		String filterPastReimbursements = getParam("filterPast");
		setSessionAttribute("filterPast", filterPastReimbursements.toLowerCase());
		refreshReimbursementPage();
	}
	
	public void process() {
		
		System.err.println(request.getRequestURI());
		switch ( request.getRequestURI() ) {
			case "/ERS/example.do": exampleRequest(); break;
			case "/ERS/login.do": login(); break;
			case "/ERS/secure/logout.do": logout(); break;
			case "/ERS/secure/addUser.do": addUser(); break;
			case "/ERS/secure/newRequest.do": newReimbRequest(); break;
			case "/ERS/secure/addReceipt.do": addReceipt(); break;
			case "/ERS/secure/viewReceipt.do": viewReceipt(); break;
			case "/ERS/secure/resolve.do": resolve(); break;
			case "/ERS/secure/updateShown.do": updateShown(); break;
			default: throw new IllegalArgumentException("Not a valid URI: " + request.getRequestURI());
		}
	}

}
