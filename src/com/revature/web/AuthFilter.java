package com.revature.web;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AuthFilter implements Filter {

	@Override
	public void destroy() {
		
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest)request;
		HttpSession session = req.getSession();
		if ( session != null ) {
			Object loggedInAttribute = session.getAttribute("loggedIn");
			if ( loggedInAttribute != null ) {
				chain.doFilter(request, response);
				return;
			}
		}
		
		// If not logged in...
		((HttpServletResponse)response).sendRedirect("/ERS/");
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		
	}

}
